import React from 'react';
import './App.css';
import Customers from './components/customers/customer';

function App() {
  return (
    <div className="App">
      <Customers></Customers>
    </div>
  );
}

export default App;
