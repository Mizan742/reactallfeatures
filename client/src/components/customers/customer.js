import React, { Component } from 'react';
import axios from 'axios';
import '../customers/customer.css';

class Customers extends Component {
    constructor() {
        super();
        this.state = {
            customers: [],
            inputShow: false           
        };
    }

    componentDidMount() {
        fetch('/api/customers')
            .then(response => response.json())
            .then(customers => this.setState({ customers }, () => console.log('Customers fetched...', customers)));
    }

    addUser = e => {
        e.preventDefault();
        this.setState({ inputShow: true });
    }

    getFormValue = e =>{
        e.preventDefault();
        var i,key = [],val=[],customerObj={};

        for(i=0;i<e.target.length-1;i++){
            key[i] = e.target[i].name;
            val[i] = e.target[i].value;
            customerObj[key[i]]= val[i];
        }
        console.log(customerObj);
      axios.post('/api/customers/addUsers',{
          params: customerObj})
      .then(response =>{
          console.log(response);
      });
    }

    render() {
        // const showInputFields = this.state.inputShow;
        return (
            <div>
                <h2>Customers</h2>
                <table id="customers">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email address</th>
                            <th>Address</th>
                        </tr>
                    </thead>


                    <tbody>
                        {this.state.customers.map(customer => {
                            return (
                                <tr key={customer.PersonID}>
                                    <td >{customer.PersonID}</td>
                                    <td >{customer.LastName}, {customer.FirstName}</td>
                                    <td >{customer.Email}</td>
                                    <td >{customer.Address}, {customer.City}, {customer.State}</td>
                                </tr>
                            );
                        })
                        }
                    </tbody>
                </table>
                <button type="button" className = "btn btn-primary" onClick={this.addUser} >Add user</button>
                <div style={{ display: this.state.inputShow ? "block" : "none" }}>
                    <form id="customerInfo" onSubmit={this.getFormValue}>
                        <h1>Enter customer information</h1>
                        <p></p>
                        <input className="inputWidth" type="text" placeholder = "customer ID" name ="PersonID"/><p></p>
                        <input className="inputWidth" type="text" placeholder = "First Name"  name ="FirstName"/><p></p>
                        <input className="inputWidth" type="text" placeholder = "Last Name" name ="LastName"/><p></p>
                        <input className="inputWidth" type="text" placeholder = "Email address"  name ="Email"/><p></p> 
                        <input className="inputWidth" type="text" placeholder = "Address"  name ="Address"/><p></p>
                        <input className="inputWidth" type="text" placeholder = "City"  name ="City"/><p></p>
                        <input className="inputWidth" type="text" placeholder = "State" name ="State"/><p></p>
                        <input type='submit'/>


                     </form>

                </div>


            </div>
        );
    }
}

export default Customers;
